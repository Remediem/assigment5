# Trivia Game using Vue
Attempt to create a trivia game using vue.

## Background
Noroff assignment

## Getting started

Clone the project.

cd into project.

run `npm install` to install all modules.

run `npm run dev` and go to the host url.

## Usage
Go to the host url. Provide a username and select the quiz settings (category, difficulty, etc.).
Click on the `Start Game` button, and you will be taken to the questions page. Answer the questions, and submit.
Each correct answer counts 10 points towards your total score. The highest score achieved on a given username will stored
using the `Trivia API`.

You can also visit a hosted version of this project on:
`https://limitless-eyrie-76663.herokuapp.com/`.



## Authors
Sigurd Skaga
