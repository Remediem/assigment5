// Trivia API category request response
export interface QuestionCategoryResponse
{
	trivia_categories: QuestionCategory[];
}

// Question category interface
export interface QuestionCategory
{
	id: number;
	name: string
}