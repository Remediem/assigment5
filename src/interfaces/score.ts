// Trivia API user score response interface
export interface UserScoreResponse
{
	id: number
	username: string
	highScore: number
}

// User score interface
export interface UserScore
{
	username: string
	highScore: number
}

