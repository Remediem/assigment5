import { Question } from "./question";
import { ResponseError } from "./error";
import { QuestionsConfig } from "./questionsConfig";
import { QuestionCategory } from "./category";

export interface State
{
	username: string;
	questionsResponse: [ResponseError, Question[]];
	userAnswers: {id: string, answer: string | null}[];
	score: number;
	config: QuestionsConfig;
	highScore: number;
	categories: QuestionCategory[];
}