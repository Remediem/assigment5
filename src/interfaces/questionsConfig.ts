export interface QuestionsConfig
{
	count: number,
	category: number,
	difficulty: string,
	type: string
}