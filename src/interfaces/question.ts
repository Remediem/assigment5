export interface QuestionResponse {
	response_code : number;
	results : Question[];
}

export interface Question {
	id: string;
	type: "boolean" | "multiple";
	question : string;
	correct_answer: string;
	incorrect_answers: string[];
}

export interface QuestionAnswer
{
	id: string;
	answer: string | null;
}