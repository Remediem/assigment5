// Generic HTTP request response error interface
export interface ResponseError
{
	message: string | null
;}