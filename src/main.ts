import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/router";
import store, { key } from "./store/store";
import './assets/css/base.css'

// Create app
createApp(App)
	.use(store, key)
	.use(router)
	.mount("#app");
