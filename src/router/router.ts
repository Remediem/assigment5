import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import StartPage from "../pages/StartPage.vue";
import GamePage from "../pages/GamePage.vue";
import ResultsPage from "../pages/ResultsPage.vue";

// Page routes
const routes: RouteRecordRaw[] = [
	{
		path: "/",
		component: StartPage,
	},
	{
		path: "/game-page",
		component: GamePage,
	},
	{
		path: "/results-page",
		component: ResultsPage,
	}
];

export default createRouter(
	{
		history: createWebHistory(),
		routes,
	});