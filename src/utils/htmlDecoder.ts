// Function to decode html into text.
export function decodeHtml(_str: string | null)
{
	if (_str != null)
	{
		const parser = new DOMParser().parseFromString(_str, 'text/html');
		return parser.documentElement.textContent;
	}
	return null;
}