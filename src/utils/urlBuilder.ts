export const questionDifficulties =
	{
		"Mixed Difficulties": "any",
		"Easy": "easy",
		"Medium": "medium",
		"Hard": "hard"
	}

export const questionTypes =
	{
		"Mixed Types": "any",
		"Multiple Choice": "multiple",
		"True / False": "boolean"
	};

// Function to build Trivia API url for a given category, difficulty and question type.
export function buildUrl(_count : number = 10, _category : number = 0, _diff : string = "", _type : string = "")
{
	let url = "https://opentdb.com/api.php?amount=" + _count;
	if (_category !== 0) url += "&category=" + _category;
	if (_diff !== "any") url += "&difficulty=" + _diff;
	if (_type !== "any") url += "&type=" + _type;
	return url;
}