import { ResponseError } from "../interfaces/error";
import { UserScore, UserScoreResponse } from "../interfaces/score";

const apiURL = "https://remediem-trivia-game.herokuapp.com";
const config = {
	headers: {
		"X-API-Key": "1234",
		'Content-Type': 'application/json'
	},
};

/*
* Function to get high scores of all usernames
*/
export async function getAllScoresAsync() : Promise<[ResponseError, UserScoreResponse[]]>
{
	try
	{
		const  requestURL = apiURL + "/trivia";
		const response = await fetch(requestURL);

		const error: ResponseError = {message: null};
		return [error, await response.json()];
	}
	catch (_err : any)
	{
		const error: ResponseError = {message: _err.message};
		return [error, []];
	}
}

/*
* Function to get high score of specific username
*/
export async function getScoreAsync(_username : string) : Promise<[ResponseError, UserScoreResponse[]]>
{
		try {
			const requestURL = apiURL + "/trivia?username=" + _username;
			const response = await fetch(requestURL);

			const error : ResponseError = {message: null};
			return [error, await response.json()];
		}
		catch (_err: any)
		{
			const error : ResponseError = {message: _err.message};
			return [error, []];
		}
}

/*
* Function to add a new username and high score
*/
export async function addScoreAsync(_userScore : UserScore) : Promise<[ResponseError, UserScoreResponse[]]>
{
	try
	{
		const requestURL = apiURL + "/trivia";
		const response = await fetch(requestURL, {
			method: 'POST',
			headers: config.headers,
			body: JSON.stringify({
				username: _userScore.username,
				highScore: _userScore.highScore
			})
		});

		const error : ResponseError = {message: null};
		return [error, await response.json()];
	}
	catch (_err : any)
	{
		const error : ResponseError = {message: _err.message};
		return [error, []]
	}
}

/*
* Function to update highScore of user by username
*/
export async function updateScoreAsync(_userScore : UserScore) : Promise<[ResponseError, UserScoreResponse[]]>
{
	try
	{
		const userId = await getUserId(_userScore.username);
		const requestURL = apiURL + "/trivia/" + userId;

		const response = await fetch(requestURL, {
			method: 'PATCH',
			headers: config.headers,
			body: JSON.stringify({
				highScore: _userScore.highScore
			})
		});
		const error : ResponseError = {message: null};
		return [error, await response.json()];
	}
	catch (_err: any)
	{
		const error : ResponseError = {message: _err.message};
		return [error, []];
	}
}

/*
* Function to delete user, and score, by username, from trivia api.
*/
export async function deleteScoreAsync(_username : string) : Promise<[ResponseError, UserScoreResponse[]]>
{
	try
	{
		const userId = await getUserId(_username);
		const requestURL = apiURL + "/trivia/" + userId;

		const response = await fetch(requestURL, {
			method: 'DELETE',
			headers: config.headers,
		});

		const error : ResponseError = {message: null};
		return [error, await response.json()];
	}
	catch (_err: any)
	{
		const error : ResponseError = {message: _err.message};
		return [error, []];
	}
}

// Get user id of specific username from trivia api
async function getUserId(_username : string) : Promise<number> {
	const userScore = (await getScoreAsync(_username));
	return userScore[1][0].id;
}
