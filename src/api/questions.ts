import axios from "axios";
import { useStore } from "../store/store";
import { Question, QuestionResponse } from "../interfaces/question";
import { buildUrl } from "../utils/urlBuilder"
import { ResponseError } from "../interfaces/error";
import { QuestionCategory, QuestionCategoryResponse } from "../interfaces/category";


// Function to generate a random string/id.
function randomId()
{
	return Math.random().toString(16).slice(2);
}

// Function to get questions with settings specified in store.
export async function fetchQuestionsAsync() : Promise<[ResponseError, Question[]]>
{
	const store = useStore();

	// Get question settings and build the correct trivia url.
	const {count, category, difficulty, type} = store.state.config;
	const TRIVIA_URL = buildUrl(count, category, difficulty, type);

	try
	{
		// Get question data and assign a random id to every question
		const { data } = await axios.get<QuestionResponse>(TRIVIA_URL);
		const questions = data.results.map(question => {return {...question, id: randomId()}});

		const error : ResponseError = {message: null};
		return [error, questions];
	}
	catch (_err : any)
	{
		const error : ResponseError = {message: _err.message};
		return [error, []];
	}
}

// Function to get all question categories.
export async function fetchCategoriesAsync() : Promise<[ResponseError, QuestionCategory[]]>
{
	const store = useStore();
	const TRIVIA_URL = "https://opentdb.com/api_category.php";

	try
	{
		const { data } = await axios.get<QuestionCategoryResponse>(TRIVIA_URL);
		const categories : QuestionCategory[] = data.trivia_categories;
		const error : ResponseError = {message: null};
		return [error, categories];
	}
	catch (_err : any)
	{
		const error : ResponseError = {message: _err.message};
		return [error, []];
	}

}
