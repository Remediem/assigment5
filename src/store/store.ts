import { createStore, Store , useStore as baseUseStore} from "vuex";
import { InjectionKey } from "vue";
import { Question, QuestionAnswer } from "../interfaces/question";
import { State } from "../interfaces/store";
import { ResponseError } from "../interfaces/error";
import { QuestionsConfig } from "../interfaces/questionsConfig";
import { QuestionCategory } from "../interfaces/category";

export const key: InjectionKey<Store<State>> = Symbol();

// Creating a store
export default createStore<State>({
	// Configuring store state
	state:
	{
		username: "",
		questionsResponse: [{message: null}, []],
		userAnswers: [],
		score: 0,
		config: {count: 10, category: 0, difficulty: "any", type: "any"},
		highScore: 0,
		categories: []
	},
	// Store mutations
	mutations:
		{
			setUsername: (_state: State, _payload: string) => _state.username = _payload,
			setQuestions: (_state: State, _payload: [ResponseError, Question[]]) => _state.questionsResponse = _payload,
			setAnswer: (_state: State, _payload: QuestionAnswer) =>
			{
				const index = _state.userAnswers.findIndex(e => e.id === _payload.id);
				if (index === -1)
					_state.userAnswers.push(_payload);
				else
					_state.userAnswers[index] = _payload;

			},
			setScore: (_state: State, _payload: number) => _state.score = _payload,
			setConfig: (_state: State, _payload: QuestionsConfig) => _state.config = _payload,
			setHighScore: (_state: State, _payload: number) => _state.highScore = _payload,
			setCategories: (_state: State, _payload: QuestionCategory[]) => _state.categories = _payload,
		}
});

export function useStore()
{
	return baseUseStore(key);
}